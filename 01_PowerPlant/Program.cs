﻿using _01_Household;

/*
 * Toto je simulátor řídícího programu pro elektrárnu na domě.
 * Simulace běží po hodinách (celkem 10 hodin) a v každé hodině elektrátna vyprodukuje 0-5kW (podle počasí)
 * V tuto chvíli se veškerá el. energie prodá do sítě.
 * Zkuste si program spustit a sledovat, jestli je to opravdu tak (pokud Vám interval zobrazování přijde dlouhý, klidně si ho snižte)
 * 
 * Nyní si program pořádně prostudujte. 
 * Spusťte.
 */

for (int i = 0; i < 10; i++)
{
    decimal plantOutput = new Random().Next(5000) / 1000m; // výkon v kW

    HouseholdModel distribution = HouseholdHelpers.HandlePlantOutput(plantOutput, household: 0);

    string gridFlow;
    if (distribution.Grid > 0)
    {
        gridFlow = "--->";
    }
    else
    {
        gridFlow = "<---";
    }

    Console.WriteLine($"Hour {i + 1}");
    Console.WriteLine($"Plant output       : {plantOutput} kW");
    Console.WriteLine($"House consumption  : {distribution.Household} kW");
    Console.WriteLine($"Grid flow          : {gridFlow} : {Math.Abs(distribution.Grid)} kW");
    Thread.Sleep(1500); // počkej 1.5  sekundy 
    Console.Clear();
}

/*
 * 01 - Přečtěte si: https://gitlab.com/school9961818/testing/-/blob/master/Testing.md
 * 
 * 02. Domácnost
 * 
 * Upravte simulaci tak, aby každou hodinu generovala náhodnou spotřebu domácnosti (0.5 - 7.0)kW.
 * Tuto spotřebu předávejte jako druhý parametr do metody HandlePlantOutput.
 * Upravte metodu tak, aby spotřebu reflektovala.
 *  Příklad: 
 *    Plant output: 2 kW, Household: 1 kW -> Grid: 1 kW, Household consumption: 1kW
 *    Plant output: 2 kW, Household: 3 kW -> Grid: -1 kW, Household consumption: 3kW
 * Po úpravě metody by Vám měly procházet první testy.
 */

