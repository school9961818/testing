﻿namespace _01_Household
{
    public static class HouseholdHelpers
    {
        /*
         * Definici této metody ani její parametry nijak neupravujte. 
         * Zkuste celé cvičení vyřešit uvnitř této metody.
         */
        public static HouseholdModel HandlePlantOutput(decimal output, decimal household)
        {
            HouseholdModel result = new HouseholdModel
            {
                Grid = output,
                Household = 0
            };

            return result;
        }
    }
}
