using _01_Household;
using _02_Pump;
using _03_Battery;
using FluentAssertions;

namespace UnitTests
{
    public class PowerPlantTests
    {

        [Theory]
        [InlineData(3.12, 0, 3.12, 0)]
        [InlineData(3.12, 1.12, 2.0, 1.12)]
        [InlineData(3.12, 4.12, -1.0, 4.12)]
        public void A_HouseholdShouldBeOk(
            decimal plantOutput,
            decimal household,
            decimal expectedGrid,
            decimal expectedHousehold)
        {
            HouseholdModel model = HouseholdHelpers.HandlePlantOutput(plantOutput, household);

            model.Grid.Should().Be(expectedGrid);
            model.Household.Should().Be(expectedHousehold);
        }


        [Theory]
        [InlineData(3.12, 0.5, 2.12, 0.5, true)]
        [InlineData(4.5, 4.3, 0.2, 4.3, false)]
        [InlineData(4.5, 5, -0.5, 5, false)]
        public void B_PumpShouldBeOk(
                decimal plantOutput,
                decimal household,
                decimal expectedGrid,
                decimal expectedHousehold,
                bool expectedPump)
        {
            PumpModel model = PumpHelpers.HandlePlantOutput(plantOutput, household);

            model.Grid.Should().Be(expectedGrid);
            model.Household.Should().Be(expectedHousehold);
            model.PumpIsOn.Should().Be(expectedPump);
        }


        [Theory]
        [InlineData(0.6, 0, 0, 0, 0.1, 0, 1, true)]
        [InlineData(0.8, 0.4, 0, 0, 0.4, 0.4, 4, false)]
        [InlineData(1, 2, 50, 0, -1.5, 2, 35, true)]
        [InlineData(0, 1, 12, 0, -1, 1, 2, false)]
        [InlineData(6, 0, 50, 0.5, 5, 0, 100, true)]
        [InlineData(0, 5, 54, 0, -5, 5, 4, false)]
        [InlineData(0, 5, 45, -0.5, -4.5, 5, 0, false)]
        public void C_BatteryShouldBeOk(
                decimal plantOutput,
                decimal household,
                int startingBatterySoC,
                decimal expectedGrid,
                decimal expectedBattery,
                decimal expectedHousehold,
                int expectedBatterySoC,
                bool expectedPump)
        {
            BatteryModel model = BatteryHelpers.HandlePlantOutput(plantOutput, household, startingBatterySoC);

            model.GridFlow.Should().Be(expectedGrid);
            model.BatteryFlow.Should().Be(expectedBattery);
            model.Household.Should().Be(expectedHousehold);
            model.PumpIsOn.Should().Be(expectedPump);
            model.BatteryStateOfCharge.Should().Be(expectedBatterySoC);
        }
    }
}