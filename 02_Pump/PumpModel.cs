﻿namespace _02_Pump
{
    public struct PumpModel
    {
        public decimal Grid;
        public decimal Household;
        public bool PumpIsOn;
    }
}
