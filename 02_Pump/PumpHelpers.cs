﻿namespace _02_Pump
{
    public static class PumpHelpers
    {
        /*
         * Definici této metody ani její parametry nijak neupravujte. 
         * Zkuste celé cvičení vyřešit uvnitř této metody.
         */
        public static PumpModel HandlePlantOutput(decimal output, decimal household)
        {
            PumpModel result = new PumpModel
            {
                Grid = output,
                Household = 0,
                PumpIsOn = false
            };

            return result;
        }
    }
}
