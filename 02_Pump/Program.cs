﻿using _02_Pump;

for (int i = 0; i < 10; i++)
{
    decimal plantOutput = new Random().Next(5000) / 1000m; // výkon v kW

    PumpModel distribution = PumpHelpers.HandlePlantOutput(plantOutput, household: 0);

    // check this out
    string gridFlow = distribution.Grid > 0 ? "--->" : "<---";
 

    Console.WriteLine($"Hour {i + 1}");
    Console.WriteLine($"Plant output       : {plantOutput} kW");
    Console.WriteLine($"House consumption  : {distribution.Household} kW");
    Console.WriteLine($"Grid flow          : {gridFlow} : {Math.Abs(distribution.Grid)} kW");
    Console.WriteLine($"Pump is on         : {distribution.PumpIsOn} - {(distribution.PumpIsOn ? "0.5" : "0")}");
    Thread.Sleep(1500); // počkej 1.5  sekundy 
    Console.Clear();
}

/*
 * 01. Přečtěte a vyzkoušejte - https://gitlab.com/school9961818/testing/-/blob/master/Debugging.md
 * 
 * 02. Domácnost a čerpadlo.
 * Upravte simulaci tak, aby používala čerpadlo.
 * Čerpadlo má výkon 0.5 kW.
 * Čerpadlo bude fungovat jen na přebytkovou (vyrobenou) energii.
 * Pokud by se pro spuštění čerpadla muselo brát ze sítě, čerpadlo bude vypnuté.
 *  Příklad: 
 *    Plant output: 2, Household: 1 -> Grid: 0.5 kW, pump is on
 *    Plant output: 1, Household: 1 -> Grid: 0 kW, pump is off
 */

