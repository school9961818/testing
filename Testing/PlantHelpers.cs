﻿namespace Testing
{
    public static class PlantHelpers
    {
        public static DistributionModel HandlePlantOutput(decimal output, decimal household = 0, bool useBattery = false, int batteryStateOfCharge = 0)
        {
            DistributionModel result = new DistributionModel
            {
                Grid = output,
                Battery = 0,
                Household = 0,
                CarCharger = 0,
                BatteryStateOfCharge = 0
            };

            return result;
        }
    }
}
