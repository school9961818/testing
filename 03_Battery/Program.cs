﻿using _03_Battery;

for (int i = 0; i < 10; i++)
{
    decimal plantOutput = new Random().Next(5000) / 1000m; // výkon v kW

    BatteryModel distribution = BatteryHelpers.HandlePlantOutput(plantOutput, household: 0, startingBatterySoC: 0);

    string gridFlow = distribution.GridFlow > 0 ? "--->" : "<---";
    string batteryFlow = distribution.BatteryFlow > 0 ? "--->" : "<---";


    Console.WriteLine($"Hour {i + 1}");
    Console.WriteLine($"Plant output       : {plantOutput} kW");
    Console.WriteLine($"House consumption  : {distribution.Household} kW");
    Console.WriteLine($"Grid flow          : {gridFlow} : {Math.Abs(distribution.GridFlow)} kW");
    Console.WriteLine($"Battery flow       : {batteryFlow} : {Math.Abs(distribution.BatteryFlow)} kW");
    Console.WriteLine($"Pump is on         : {distribution.PumpIsOn} - {(distribution.PumpIsOn ? "0.5" : "0")}");
    Console.WriteLine($"Battery SoC        : {distribution.BatteryStateOfCharge}%");
    Thread.Sleep(1500); // počkej 1.5  sekundy 
    Console.Clear();
}

/*
 * 03. Domácnost, čerpadlo a baterka.
 * 
 * Upravte simulaci tak, aby používala baterku.
 * Baterka má kapacitu 10kW.
 * Jak moc je nabitá vyjadřuje v procentech "StateOfCharge" - (40% = 4 kW)
 * Baterka má nejnižší prioritu pro nabíjení (první se pořeší domácnost, pak čerpadlo, nakonec baterka)
 * Čerpadlo může jet na baterku (ale nikdy ze sítě).
 * Baterka se nesmí přebít (nad 100%), ani jít pod 0 %. Přebytečná energie se pošle do/ze sítě.
 *  Příklad: 
 *    Plant output: 2, Household: 1, batterySoC: 0 -> Grid: 0 kW, pump is on, batterySoC: 5 % 
 */

