﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_Battery
{
    public struct BatteryModel
    {
        public decimal GridFlow;
        public decimal BatteryFlow;
        public decimal Household;
        public bool PumpIsOn;
        public int BatteryStateOfCharge;
    }
}
