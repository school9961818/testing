﻿

namespace _03_Battery
{
    public static class BatteryHelpers
    {
        /*
         * Definici této metody ani její parametry nijak neupravujte. 
         * Zkuste celé cvičení vyřešit uvnitř této metody.
         */
        public static BatteryModel HandlePlantOutput(decimal output, decimal household, int startingBatterySoC)
        {
            BatteryModel result = new BatteryModel
            {
                GridFlow = output,
                BatteryFlow = 0,
                Household = 0,
                PumpIsOn = false,
                BatteryStateOfCharge = 0,
            };

            return result;
        }
    }
}
